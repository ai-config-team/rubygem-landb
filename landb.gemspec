require 'rake'

Gem::Specification.new do |s|
  s.name        = 'landb'
  s.version     = '0.1.5'
  s.date        = '2012-08-17'
  s.summary     = "This is a WSDL dynamic wrapper for lanDB to puppet use."
  s.description = "This is a WSDL dynamic wrapper for lanDB to puppet use. It's writen from Anastasis Andronidis as an OpenLab Summer Student project"
  s.authors     = ["Anastasios Andronidis, Daniel Lobato Garcia, Alberto Rodriguez Peon"]
  s.email       = 'anastasis90@yahoo.gr'
  s.files       = ['lib/landb.rb','lib/landb/landb_client.rb','lib/landb/landb_responce.rb','landb.gemspec']
  s.homepage    = 'https://gitlab.cern.ch/ai-config-team/rubygem-landb'
end
