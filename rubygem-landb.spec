%global gem_name landb

Name:           rubygem-%{gem_name}
Version:        0.1.5
Release:        1%{?dist}
Summary:        This is a WSDL dynamic wrapper for lanDB to puppet use
License:        ASL 2.0
URL:            https://gitlab.cern.ch/ai-config-team/landb-gem
Source0:        landb-%{version}.gem

Requires:       ruby(release)
Requires:       ruby(rubygems)
Requires:       ruby
Requires:       rubygem(savon) >= 2

BuildRequires:  ruby(release)
BuildRequires:  rubygems-devel 
BuildRequires:  rubygem-rake 
BuildArch:      noarch

%description
This is a WSDL dynamic wrapper for lanDB to puppet use. It's writen from
Anastasis Andronidis as an OpenLab Summer Student project.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n %{gem_name}-%{version}


%build
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%files
%dir %{gem_instdir}
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}
# Must be a macro for this.
/usr/share/gems/gems/landb-%{version}/landb.gemspec

%files doc
%{gem_docdir}

%changelog
* Mon Oct 11 2021 Steve Traylen <steve.traylen@cern.ch> - 0.1.5-1
- Stop using URI.open

* Wed Apr 28 2021 Steve Traylen <steve.traylen@cern.ch> - 0.1.4-1
- Add back missing landb.gem file.

* Wed Apr 28 2021 Steve Traylen <steve.traylen@cern.ch> - 0.1.3-1
- Bump for firest CC8 8 build

* Mon May 23 2016 Steve Traylen <steve.traylen@cern.ch> - 0.1.2-2
- Redo spec file from scratch to support scl at same time
  for next time.

* Tue Feb 17 2015 Steve Traylen <steve.traylen@cern.ch> - 0.1.2-1
- Up 0.1.2 , fix for authenticated called on CC7.

* Thu Oct 9 2014 Steve Traylen <steve.traylen@cern.ch> - 0.1.0-1
- Up 0.1.1 , the one that supports savon2.

* Fri Aug 15 2014 Fedora Cloud User <steve.traylen@cern.ch> - 0.0.4-1
- Initial package
