SPECFILE            = $(shell find -maxdepth 1 -type f -name *.spec)
DIST               ?= $(shell rpm --eval %{dist})

sources:
	gem build landb.gemspec

clean:
	rm -f *.gem

rpm: sources
	rpmbuild -bb --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)

srpm: sources
	rpmbuild -bs --define "dist $(DIST)" --define "_topdir $(PWD)/build" --define '_sourcedir $(PWD)' $(SPECFILE)
